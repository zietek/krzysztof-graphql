package com.example.krzysztofgraphql.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataModel {

    private String _id;

    private String name;

    private transient List<String> usedIn = new ArrayList<>();

    private String timestampFormat;

    private List<String> timestampFields = new ArrayList<>();

    private Date createdAt;
    private Date modifiedAt;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUsedIn() {
        return usedIn;
    }

    public void setUsedIn(List<String> usedIn) {
        this.usedIn = usedIn;
    }

    public String getTimestampFormat() {
        return timestampFormat;
    }

    public void setTimestampFormat(String timestampFormat) {
        this.timestampFormat = timestampFormat;
    }

    public List<String> getTimestampFields() {
        return timestampFields;
    }

    public void setTimestampFields(List<String> timestampFields) {
        this.timestampFields = timestampFields;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }
}