package com.example.krzysztofgraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KrzysztofGraphqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(KrzysztofGraphqlApplication.class, args);
	}

}
