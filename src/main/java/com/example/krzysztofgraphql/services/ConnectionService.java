package com.example.krzysztofgraphql.services;

import com.example.krzysztofgraphql.model.Connection;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ConnectionService {
    public Connection getConnection(String id) {
        Connection result = new Connection();
        result.set_id("id");
        result.setName("dupa");
        return result;
    }

    public List<Connection> getConnections(List<String> sort, int page, int size, String name) {
        Connection result = new Connection();
        result.set_id("id_z_listy");
        result.setName("dupa_z_listy");
        return Collections.singletonList(result);
    }

}
