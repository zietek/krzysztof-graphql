package com.example.krzysztofgraphql.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.krzysztofgraphql.model.Connection;
import com.example.krzysztofgraphql.services.ConnectionService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class QueryResolver implements GraphQLQueryResolver {

    private ConnectionService connectionService;

    public QueryResolver(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public Connection getConnection(String id) {
        return connectionService.getConnection(id);
    }

    public List<Connection> getConnections(String name, List<String> sort, int page, int size) {

        return connectionService.getConnections(sort, page, size, name);
    }

}
